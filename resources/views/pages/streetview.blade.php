@extends('layouts.street')

@section('scripts')
<script>
    function initialize() {
        var fenway = {lat: 42.345573, lng: -71.098326};
        var map = new google.maps.Map(document.getElementById('map'), {
          center: fenway,
          zoom: 14
        });
        var panorama = new google.maps.StreetViewPanorama(
            document.getElementById('pano'), {
              position: fenway,
              pov: {
                heading: 34,
                pitch: 10
              }
            });
        map.setStreetView(panorama);
      }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbZjM06jw_g1sm_O9kIu_nV2cmtQIwmuo&callback=initialize">
</script>
@endsection
