@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(count($user) > 0)
                    <h3>Profile</h3>
                    <ul class="list-group">
                        <li class="list-group">ID: {{$user->id}}</li>
                        <li class="list-group">Name: {{$user->username}}</li>
                        <li class="list-group">Name: {{$user->email}}</li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection