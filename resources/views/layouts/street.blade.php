<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Manadium') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
      html, body {
        height: 320;
        margin: 0;
        padding: 0;
      }
      #map, #pano {
        float: left;
        height: 650px;
        width: 49%;
      }
    </style>
  </head>
  <body>
    <div id="app">
        @include('inc.navbar')
        <div id="map"></div>
        <div id="pano"></div>
    </div>
    <script>
    function initialize() {
        var fenway = {lat: 48.9245045, lng: 2.359845};
        var map = new google.maps.Map(document.getElementById('map'), {
          center: fenway,
          scrollWheel: false,
          zoomControl: false,
          mapTypeControl: false,
          draggable: false,
          zoom: 17
        });
        var panorama = new google.maps.StreetViewPanorama(
            document.getElementById('pano'), {
              position: fenway,
              pov: {
                heading: 34,
                pitch: 10
              }
            });
        map.setStreetView(panorama);
      }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbZjM06jw_g1sm_O9kIu_nV2cmtQIwmuo&callback=initialize">
    </script>
  </body>
</html>