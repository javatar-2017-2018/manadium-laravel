@extends('layouts.app')

@section('content')
    <h1>Golden Book Posts</h1>
    <a href="/posts/create" class="btn btn-primary">Create post</a>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="well">

                <div class="row">
                    <div class="col-md-4 col-sm4">
                        <img style="width:50%" src="/storage/cover_images/{{$post->cover_image}}">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/posts/{{$post->id}}">{{$post->title}}</h3></a>
                        <small>Written on {{$post->created_at}} by <a href="/users/{{$post->user_id}}">{{$post->user->username}}</a></small>
                    </div>
                </div>
                
            </div>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No posts found</p>
    @endif
@endsection