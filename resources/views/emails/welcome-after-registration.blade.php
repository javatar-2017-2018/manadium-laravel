@component('mail::message')
# Welcome {{ $user->username }}

Thanks so much for registering!

@component('mail::button', ['url' => "https://manadium/confirmation/{$user->id}/{$user->confirmation_token}" ])
Valid your email
@endcomponent

@component('mail::panel', ['url' => ''])
The new stadium manager
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
