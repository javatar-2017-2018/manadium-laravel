@extends('layouts.app')

@section('content')
    <h1>Users</h1>
    @if(count($users) > 0)
        @foreach($users as $user)
            <div class="well">

                <div class="row">
                    <div class="col-md-4 col-sm4">
                        <img style="width:15%" src="/storage/users/avatar/{{$user->avatar}}">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/users/{{$user->id}}">{{$user->username}}</h3></a>
                        <p>{{$user->nickname}}</p>
                        @if(!Auth::guest())
                            {{--@TODO Allow for admin group--}}
                            @if(Auth::user()->id == $user->id || Auth::user()->id == 1)  
                                <a href="/users/{{$user->id}}/edit" class="btn btn-default">Edit</a>

                                {!!Form::open(['action' => ['Back\UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                {!!Form::close()!!}
                            @endif
                        @endif
                    </div>
                </div>

            </div>
        @endforeach
        {{$users->links()}}
    @else
        <p>No users found</p>
    @endif
@endsection