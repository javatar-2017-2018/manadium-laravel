@extends('layouts.app')

@section('content')
    <h1>Edit {{$user->username}}</h1>
    {!! Form::open(['action' => ['PostsController@update', $user->id], 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('username', 'Username')}}
            {{Form::text('username', $user->username, ['class' => 'form-control', 'placeholder' => 'Username'])}}
        </div>

        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Save', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection