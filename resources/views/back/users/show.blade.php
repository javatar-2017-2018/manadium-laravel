@extends('layouts.app')

@section('content')
    <a href="/users" class="btn btn-default">Go Back</a>
    <h1>{{$user->username}}</h1>
    <img style="width:20%" src="/storage/users/avatar/{{$user->avatar}}">
    <br><br>
    <div>
        <p> User number : {!!$user->id!!} </p>
        <p> Username : {!!$user->username!!}</p>
        <p> Last name : {!!$user->last_name!!}</p>
        <p> First name : {!!$user->first_name!!}</p>
        <p> Born date : {!!$user->born_year!!}</p>
        <p> Gender : {!!$user->gender!!}</p>
        <p> Mail adress : {!!$user->email!!}</p>
        <p> Phone number : {!!$user->num_phone!!}</p>
        <p> Town : {!!$user->town!!}</p>
        <p> Street : {!!$user->street!!}</p>
        <p> Postal code : {!!$user->cp!!}</p>
        <p> Inscription date : {!!$user->date_inscription!!}</p>

    </div>
    <hr>
    {{--<small>Written on {{$post->created_at}}</small>--}}
    <hr>
    @if(!Auth::guest())
        {{--@TODO Allow for admin group--}}
        @if(Auth::user()->id == $user->id)
            <a href="/users/{{$user->id}}/edit" class="btn btn-default">Edit</a>

            {!!Form::open(['action' => ['Back\UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection