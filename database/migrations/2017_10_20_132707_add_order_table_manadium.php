<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id_order');
            $table->integer('id')->unsigned();
            $table->integer('id_etat')->unsigned();
            $table->integer('id_address_liv')->unsigned();
            $table->date('date_order');
            $table->decimal('cost_tot_order',6,2);
            $table->decimal('cost_liv_order',6,2);

            $table->foreign('id')->references('id')->on('users');
            $table->foreign('id_etat')->references('id_etat')->on('etat');
            $table->foreign('id_address_liv')->references('id_address_liv')->on('address_livraison');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
