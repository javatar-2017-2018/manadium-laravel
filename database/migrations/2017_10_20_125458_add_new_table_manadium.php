<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new', function (Blueprint $table) {
            $table->increments('id_news');
            $table->integer('id')->unsigned();
            $table->string('title_news');
            $table->date('date_publication_news');
            $table->date('date_last_modif_news');
            $table->date('date_creation_news');
            $table->boolean('visible_news');
            $table->string('description_news');
            $table->string('picture_news');

            $table->foreign('id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new');
    }
}
