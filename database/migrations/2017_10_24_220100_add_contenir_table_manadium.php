<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContenirTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contenir', function (Blueprint $table) {
            $table->integer('id_article')->unsigned();
            $table->integer('id_order')->unsigned();
            $table->integer('nb_articles');
            $table->boolean('envoi_article_courrier_selectionne');
            $table->boolean('envoi_article_email_selectionne');

            $table->primary(['id_article','id_order']);
            $table->foreign('id_article')->references('id_article')->on('article');
            $table->foreign('id_order')->references('id_order')->on('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contenir');
    }
}
