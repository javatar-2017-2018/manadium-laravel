<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('username')->unique();
            $table->string('password');
            $table->integer('born_year')->nullable();
            $table->char('gender',1)->nullable();
            $table->string('email')->unique();
            $table->char('num_phone',10)->nullable();
            $table->string('town')->nullable();
            $table->string('street')->nullable();
            $table->char('cp',5)->nullable();
            $table->string('hash')->nullable();
            $table->date('date_inscription')->nullable();
            $table->ipAddress('last_ip')->nullable();
            $table->string('avatar')->nullable();
            $table->string('confirmation_token', 60);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
