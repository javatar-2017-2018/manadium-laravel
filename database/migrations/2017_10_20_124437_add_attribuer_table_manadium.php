<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttribuerTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribuer', function (Blueprint $table) {
            $table->integer('id_user')->unsigned();
            $table->integer('id_group')->unsigned();
            $table->date('date_debut_groupe');
            $table->date('date_expi_groupe');

            $table->primary(['id_user','id_group']);
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_group')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribuer');
    }
}
