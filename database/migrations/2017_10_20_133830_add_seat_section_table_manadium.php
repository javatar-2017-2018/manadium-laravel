<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeatSectionTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seatsection', function (Blueprint $table) {
            $table->increments('id_seatsection');
            $table->integer('id_place')->unsigned();
            $table->string('description_seatsection');

            $table->foreign('id_place')->references('id_place')->on('place');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seat_section');
    }
}
