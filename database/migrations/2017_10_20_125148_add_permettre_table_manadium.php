<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermettreTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permettre', function (Blueprint $table) {
            $table->integer('id_group')->unsigned();
            $table->integer('id_permission')->unsigned();
            $table->date('date_debut_permission');
            $table->date('date_fin_permission');

            $table->foreign('id_group')->references('id')->on('groups');
            $table->foreign('id_permission')->references('id_permission')->on('permission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permettre');
    }
}
