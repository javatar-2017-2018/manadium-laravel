<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArticleTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('id_article');
            $table->integer('id_event')->unsigned();
            $table->integer('id_category')->unsigned();
            $table->string('lib_article');
            $table->float('prix_article',8,2);
            $table->string('description_article');
            $table->integer('quantite_article');
            $table->integer('poids_article');
            $table->integer('stock_article');
            $table->boolean('envoi_email_article');
            $table->boolean('envoi_courrier_article');

            $table->foreign('id_event')->references('id_event')->on('event');
            $table->foreign('id_category')->references('id_category')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
