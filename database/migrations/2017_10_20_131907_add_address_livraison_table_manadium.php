<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressLivraisonTableManadium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_livraison', function (Blueprint $table) {
            $table->increments('id_address_liv');
            $table->integer('id')->unsigned();
            $table->string('pays_address_liv');
            $table->string('nom_address_liv');
            $table->string('prenom_address_liv');
            $table->string('ap_address_liv');
            $table->string('ap2_address_liv');
            $table->string('ville_address_liv');
            $table->char('cp_address_liv',5);
            $table->char('num_phone_address_liv',10);

            $table->foreign('id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_livraison');
    }
}
