<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        //Group::truncate();

        $faker = \Faker\Factory::create();

        Group::create([
            'type_groupe' => 'admin',
            'description_groupe' => 'admin',
        ]);

        // And now, let's create a few goups in our database:
        for ($i = 0; $i < 50; $i++) {
            Group::create([
                'type_groupe' => $faker->sentence,
                'description_groupe' => $faker->sentence,
            ]);
        }
    }
}
