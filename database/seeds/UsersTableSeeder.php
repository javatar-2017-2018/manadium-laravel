<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's clear the users table first
        //$this->command->info('message');
        //TODO: Fix truncate error
        //User::truncate();

        $faker = \Faker\Factory::create();

        // Let's make sure everyone has the same password and
        // let's hash it before the loop, or else our seeder
        // will be too slow.
        $password = Hash::make('secret');

        User::create([
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => $password,
            'confirmation_token' => '',
            'confirmed' => true,
            'avatar'=>'basic_avatar.svg',
        ]);

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'username' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
                'confirmation_token' => '',
                'avatar'=>'basic_avatar.svg',
                'confirmed' => true,
            ]);
        }
    }
}