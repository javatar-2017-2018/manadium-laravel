<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // Table name
    protected $table = 'posts';
    // Primary key
    public $primarykey = 'id';
    // Timestamp
    public $timestamps = true;

    // User relasionship
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
