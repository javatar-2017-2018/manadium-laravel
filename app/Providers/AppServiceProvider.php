<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
        Schema::defaultStringLength(191);
        // Force root url to fix issues with reverse proxy
        if (!empty($proxy_url)) {
            \URL::forceRootUrl($proxy_url);
        }

        // Fix https with reverse proxy
        if (!empty($proxy_schema)) {
            \URL::forceScheme($proxy_schema);
        }
    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
