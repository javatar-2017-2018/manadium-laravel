<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    // Users relationships
    public function users()
    {
        return $this->hasMany('App\Users');
    }
}
