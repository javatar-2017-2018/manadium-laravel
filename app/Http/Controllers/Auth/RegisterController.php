<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\WelcomeAfterRegistration;
use App\User;
use Bestmomo\LaravelEmailConfirmation\Traits\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '../dashboard';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     * Enable Captcha for production environments.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $debug = env('APP_DEBUG', false);

        $dataPolicyCaptcha = [
            'g-recaptcha-response'=> 'required',
        ];

        $dataPolicy = [
            'username'            => 'required|string|max:255|alpha_num|unique:users',
            'email'               => 'required|string|email|max:255|unique:users',
            'password'            => 'required|string|min:6|confirmed',
        ];

        // If in production environment, add recaptcha
        if ($debug) {
            $dataPolicy = array_merge($dataPolicyCaptcha, $dataPolicy);
        }

        return Validator::make($data, $dataPolicy);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        $token = str_random(60);

        $user = User::create([
            'username'           => $data['username'],
            'email'              => $data['email'],
            'confirmation_token' => $token,
            'password'           => bcrypt($data['password']),
            'avatar'             => 'basic_avatar.svg',
        ]);

        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed                    $user
     *
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        Mail::to($user)->send(new WelcomeAfterRegistration($user));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect('../')->with('success', 'Your account has been created, please valid your email.');
    }

    public function confirm(Request $request, $user_id, $token)
    {
        $user = User::findOrFail($user_id);
        if ($user->confirmation_token === $token && $user->confirmed == false) {
            $user->confirmation_token = '0';
            $user->confirmed = true;
            $user->save();
        } else {
            return redirect('../')->with('error', 'Token is not valid.');
        }
        $this->guard()->login($user);

        return redirect('../dashboard')->with('success', 'Your account has been validated.');
    }
}
