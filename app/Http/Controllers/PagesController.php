<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function index()
    {
        $title = 'Welcome to Manadium';
        //return view ('pages.index', compact('title'));
        return view('pages.index')->with('title', $title);
    }

    public function about()
    {
        $title = 'About us';

        return view('pages.about')->with('title', $title);
    }

    public function services()
    {
        $data = [
            'title'    => 'Services',
            'services' => ['Web design', 'Programming', 'SEO']
        ];

        return view('pages.services')->with($data);
    }
    public function street()
    {
        $title = 'Visite virtuelle';
        
        return view('pages.streetview')->with('title',$title);
    }
}
