.PHONY: install help test-format test server composer-update composer-install composer-install-dev npm-install-dev npm-install clean env-dev env-prod seed publish link
.DEFAULT_GOAL= help

COM_COLOR   = \033[0;34m
OBJ_COLOR   = \033[0;36m
OK_COLOR    = \033[0;32m
ERROR_COLOR = \033[0;31m
WARN_COLOR  = \033[0;33m
NO_COLOR    = \033[m

HOST?=127.0.0.1
PORT?=8000
PHP?=php
COMPOSER?=composer

CURRENT_DIR= $(shell pwd)

composer-install: composer.json ## Install all the required composer packages
	$(COMPOSER) install --no-interaction --no-dev --optimize-autoloader

composer-install-dev: composer.json ## Install all the required composer packages seed
	$(COMPOSER) install --no-interaction

composer-update: composer.json ## Update all the required composer packages
	$(COMPOSER) update

clear-cache: ## Clear composer cache
	$(COMPOSER) clear-cache

npm-install-dev: package.json ## Install and compile all developpement dependecies from npm
	npm install
	npm run dev

npm-install: package.json ## Install and compile all production dependecies from npm
	npm install
	npm run prod

install: composer-install npm-install publish link clear-cache cache ## Install/update all the required composer packages

install-dev: composer-install-dev npm-install-dev publish link ## Install/update all the required composer packages

build:
	docker-compose build
	docker-compose up -d 

env-prod:
	$(PHP) artisan config:clear
	rm -f .env
	cp -f .env.production .env

env-dev:
	$(PHP) artisan config:clear
	rm -f .env
	cp -f .env.development .env


test: install ## Start unit test
	vendor/bin/phpunit --configuration phpunit.xml --coverage-text

test-format: install ## Test code formatting
	vendor/bin/phpcs -s
	vendor/bin/php-cs-fixer fix --diff --dry-run

server: install-dev  ## Start the built-in php artisan web server
	$(PHP) artisan serve --port $(PORT) --host $(HOST)

clean: ## Clean out laravel's cache, logs, session and storage directories
	rm storage/framework/views/* storage/framework/cache/* storage/framework/sessions/* storage/app/logs/*

install-composer: composer ## Install composer pacakage
	curl -sS http://getcomposer.org/installer | php
	mv comoser.phar composer

migrate: ## Run migrations
	$(PHP) artisan migrate --force

seed: ## Seed database
	$(PHP) artisan db:seed

rollback: ## Rollback a migration
	$(PHP) artisan migrate:rollback

fresh: ### Fresh a migration
	$(PHP) artisan migrate:fresh

resfresh: ## Resfresh a migration
	$(PHP) artisan migrate:refresh

key: ## Generate key
	$(PHP) artisan key:generate --force

publish: ## Publish dependencies
	$(PHP) artisan vendor:publish --tag=confiration:translations --force
	$(PHP) artisan vendor:publish --tag=ckeditor --force
	$(PHP) artisan vendor:publish --tag=laravel-mail --force
	$(PHP) artisan vendor:publish --tag=laravel-notification --force
	$(PHP) artisan vendor:publish --tag=laravel-pagination --force

link: ## Link storage with public
	rm -rvf public/storage
	ln -sr storage/app/public public/storage

cache: ## Optimize for production
	$(PHP) artisan config:cache
	##$(PHP) artisan route:cache

help: ## Show help
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


