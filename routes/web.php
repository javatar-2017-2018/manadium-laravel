<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$proxy_url = getenv('PROXY_URL');
$proxy_schema = getenv('PROXY_SCHEMA');

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');
Route::get('/street','PagesController@street');

Route::resource('/users', 'Back\UsersController');

Route::resource('posts', 'PostsController');

/*
Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users/{id}/{name}', function ($id, $name) {
    return 'This is user '.$name.' with an id of '.$id;
});
*/
Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
