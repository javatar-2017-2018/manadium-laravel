<?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;

class PostTest extends TestCase
{
    public function testCreateNewPost()
    {
        $post = new Post();
        $post->title = 'Title post one';
        $post->body = 'Body post one';
        $post->user_id = 1;
        $post->cover_image = 'noimage.png';
        $post->save();

        $this->assertSame('Title post one', $post->title);
        $this->assertSame('Body post one', $post->body);
        $this->assertSame(1, $post->user_id);
        $this->assertSame('noimage.png', $post->cover_image);
    }
}
