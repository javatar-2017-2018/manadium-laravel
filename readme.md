#Manadium


## About Manadium

Manadium is a web application based on the Laravel framework, 

## Pre-required

* \>= php7.1
* composer

## Setup

First of all you need to copy the `.env` config file. It will let you manage your project settings.
```bash
cp .env.example .env
```

After that, we will need to generate your unique key, to do so:
```bash
php artisan key:generate
```

And now, run some compose magics to downloads the vendor files:
```bash
composer install
```

Once your config file properly defined, we will need to setup your database, just run:
```bash
php artisan migrate
```

Enable access for ckeditor:
```bash
php artisan vendor:publish --tag=ckeditor
```
